import java.util.Arrays;
import java.util.Scanner;

public class Hangman{
    public static void main(String[] args) {
        String[] palavras = {"leite", "caderno", "programa"};
        // Pick random index of palavras array
        int randomNumPal = (int) (Math.random() * palavras.length);
        // Create an array to store already entered letters
        char[] enteredLetters = new char[palavras[randomNumPal].length()];
        int triesCount = 0;
        boolean wordIsGuessed = false;
        do {
        // infinitely iterate through cycle as long as enterLetter returns true
        // if enterLetter returns false that means user guessed all the letters
        // in the word e. g. no asterisks were printed by printWord
        switch (enterLetter(palavras[randomNumPal], enteredLetters)) {
            case 0:
                triesCount++;
                break;
            case 1:
                triesCount++;
                break;
            case 2:
                break;
            case 3:
                wordIsGuessed = true;
                break;
        }
         
        }